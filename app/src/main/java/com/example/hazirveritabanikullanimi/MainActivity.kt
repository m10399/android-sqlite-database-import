package com.example.hazirveritabanikullanimi

import android.os.Bundle
import android.util.Log
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import com.info.sqlitekullanimihazirveritabani.DatabaseCopyHelper

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        setContentView(R.layout.activity_main)
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main)) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }
        veritabaniKopyala()

        val vt = VeriTabaniYardimcisi(this)

        //val liste = Kategorilerdao().tumKategoriler(vt)
        try {
            val liste = Filmlerdao().tumKategorilerByKategoriId(vt,2)
            for (f in liste){
                Log.e("Film id",(f.film_id).toString())
                Log.e("Film ad",(f.film_ad))
                Log.e("Film yıl",(f.film_yil).toString())
                Log.e("Film resim",(f.film_resim))
                Log.e("Film Kategori id",(f.kategori.kategori_id).toString())
                Log.e("Film Kategori ad",(f.kategori.kategori_ad))
                Log.e("Film Yonetmen id",(f.yonetmen.yonetmen_id).toString())
                Log.e("Film Yonetmen ad",(f.yonetmen.yonetmen_ad))
            }
        }catch (e:Exception){
            e.printStackTrace()
        }

    }
    fun veritabaniKopyala(){
        val db = DatabaseCopyHelper(this)
        try {
            db.createDataBase()
        }catch (e:Exception){
            e.printStackTrace()
        }
        try {
            db.openDataBase()
        }catch (e:Exception){
            e.printStackTrace()
        }


    }
}